package edu.byui.cs246.crt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This class defines the support currency pairs.
 */

public class Config {

    public static final List<String> ENABLED_SYMBOLS =
            Collections.unmodifiableList(Arrays.asList("BTCUSDT", "BCCUSDT","ETHUSDT","NEOUSDT","LTCUSDT"));

}
