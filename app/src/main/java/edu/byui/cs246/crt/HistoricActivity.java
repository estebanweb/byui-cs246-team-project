package edu.byui.cs246.crt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import edu.byui.cs246.crt.adapters.HistoricCursorAdapter;

/**
 * This class initiates the Operation Log (History) screen.
 */
public class HistoricActivity extends AppCompatActivity {

    protected TextView mTextView;

    /**
     * Creates the Operations Log (History) screen
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_s_sign);
        }

        // Find ListView to populate
        ListView transactionLog = findViewById(R.id.transactionLog);
        // Setup cursor adapter using cursor from last step
        HistoricCursorAdapter adapter = new HistoricCursorAdapter(this, PurchaseHistoryDbHelper.getHistoricLog(getApplicationContext()));

        transactionLog.setAdapter(adapter);

        mTextView = findViewById(R.id.mainText);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Define navegability from the menu in upper right of the screen
     * @param item Menu object from the top right on the screen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_analysis) {
            Intent intent = new Intent(HistoricActivity.this, AnalysisActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_convert) {
            Intent intent = new Intent(HistoricActivity.this, ConverterActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_historic) {
            return true;
        }

        if (id == R.id.action_refresh) {
            //NavUtils.navigateUpFromSameTask(this);
            //return true;
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

}
