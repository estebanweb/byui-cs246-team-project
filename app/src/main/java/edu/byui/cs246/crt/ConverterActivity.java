package edu.byui.cs246.crt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.binance.api.client.domain.market.TickerPrice;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * This class initiates the Convert feature screen.
 */
public class ConverterActivity extends AppCompatActivity {

    EditText amountFrom;
    EditText amountTo;
    ArrayList<TickerPrice> listPrices;
    Spinner currencyFrom;
    Spinner currencyTo;
    Gson gson;
    boolean avoidDeadLock = false;

    /**
     * Creates the Convert screen
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_s_sign);
        }

        SharedPreferences sharedpreferences = getSharedPreferences("SYMBOLVALUES", Context.MODE_PRIVATE);
        String values = sharedpreferences.getString("VALUES",null);

        Type type = new TypeToken<List<TickerPrice>>() {}.getType();
        gson = new Gson();

        listPrices =  gson.fromJson(values, type);

        List<String> list = new ArrayList<>();

        for (TickerPrice price: listPrices) {
            list.add(price.getSymbol());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        currencyFrom = findViewById(R.id.currencyFrom);
        currencyTo = findViewById(R.id.currencyTo);

        currencyFrom.setAdapter(adapter);
        currencyTo.setAdapter(adapter);

        //put in for default in the next
        currencyTo.setSelection(1);


        amountFrom = findViewById(R.id.amountFrom);
        amountTo = findViewById(R.id.amountTo);

        amountFrom.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                updateConversion(false);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        amountTo.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                updateConversion(true);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

    }

    /**
     * Update the conversion value, once the numbers are changed.
     * @param reverse if true, convert values in reverse
     */
    @SuppressLint("DefaultLocale")
    public void updateConversion(boolean reverse){
        if(avoidDeadLock){
            return;
        }
        avoidDeadLock = true;
        Double valueFrom = Double.parseDouble(listPrices.get(currencyFrom.getSelectedItemPosition()).getPrice());
        Double valueTo = Double.parseDouble(listPrices.get(currencyTo.getSelectedItemPosition()).getPrice());
        String fs = amountFrom.getText().toString();
        EditText target = amountTo;

        if (reverse){
            Double pivot = valueFrom;
            valueFrom = valueTo;
            valueTo = pivot;
            fs = amountTo.getText().toString();
            target = amountFrom;
        }

        Double newValue = 0.00;
        if (!fs.isEmpty()){
            Double from = Double.parseDouble(fs);
            newValue = (from * valueFrom)/valueTo;
        }

        target.setText(String.format("%.2f",Float.parseFloat(newValue.toString())));
        avoidDeadLock = false;
    }


    /**
     * Create currencies menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Define navegability from the menu in upper right of the screen
     * @param item Menu object from the top right on the screen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_analysis) {
            Intent intent = new Intent(this, AnalysisActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_historic) {
            Intent intent = new Intent(this, HistoricActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_convert) {
            return true;
        }

        if (id == R.id.action_refresh) {
            //NavUtils.navigateUpFromSameTask(this);
            //return true;
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
