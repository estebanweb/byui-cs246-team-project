package edu.byui.cs246.crt.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.binance.api.client.domain.market.TickerPrice;
import java.util.ArrayList;
import edu.byui.cs246.crt.R;

/**
 * This class deals with the Symbol array adapter
 */

public class SymbolArrayAdapter extends ArrayAdapter<TickerPrice> {

    private final Context context;
    private final ArrayList<TickerPrice> values;


    public SymbolArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<TickerPrice> objects) {
        super(context, resource, objects);
        this.context = context;
        this.values = objects;
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("ViewHolder") View rowView = inflater.inflate(R.layout.rowlayout, parent, false);


        TextView labelView = rowView.findViewById(R.id.label);
        TextView valueView = rowView.findViewById(R.id.value);

        TickerPrice row = values.get(position);

        labelView.setText(row.getSymbol());
        valueView.setText("$" + String.format("%.2f",Float.parseFloat(row.getPrice())));

        return rowView;
    }

    public ArrayList<TickerPrice> getValues() {
        return values;
    }

}
