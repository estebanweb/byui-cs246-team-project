package edu.byui.cs246.crt;

import android.provider.BaseColumns;

/**
 * This class define the database structure.
 * Created by esteban on 4/2/18.
 */
public final class PurchaseHistoryContract {

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private PurchaseHistoryContract() {}

    /* Inner class that defines the table contents */
    public static class HistoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "purchaseHistory";
        public static final String COLUMN_NAME_BUYDATE = "buyDate";
        public static final String COLUMN_NAME_SELLDATE = "sellDate";
        public static final String COLUMN_NAME_PAIR = "pair";
        public static final String COLUMN_NAME_BUYPRICE = "buyPrice";
        public static final String COLUMN_NAME_SELLPRICE = "sellPrice";
        public static final String COLUMN_NAME_RATE = "rate";
        public static final String COLUMN_NAME_STATUS = "status";

    }
}
