package edu.byui.cs246.crt;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * This class initiates the Market Parameter screen
 */
public class MarketParametersActivity extends AppCompatActivity {


    /**
     * Create Market Parameters Activity Screen.
     * Automatically retrieve the current parameters saved in
     * shared preferences when the application was started
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketparameters);

        //Show Toast
        showToast("Loading Current Parameters...");

        SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams",Context.MODE_PRIVATE);
        String gapMinDef = sharedPref.getString("gapMin","");
        String gapMaxDef = sharedPref.getString("gapMax","");
        String stopLossDef = sharedPref.getString("stopLoss","");
        String buyTargetDef = sharedPref.getString("buyTarget","");
        String sellTargetDef = sharedPref.getString("sellTarget","");
        String timeWindowDef = sharedPref.getString("timeWindow","");

        ((EditText) findViewById(R.id.gapMin)).setText(gapMinDef);
        ((EditText) findViewById(R.id.gapMax)).setText(gapMaxDef);
        ((EditText) findViewById(R.id.stopLoss)).setText(stopLossDef);
        ((EditText) findViewById(R.id.buyTarget)).setText(buyTargetDef);
        ((EditText) findViewById(R.id.sellTarget)).setText(sellTargetDef);
        ((EditText) findViewById(R.id.timeWindow)).setText(timeWindowDef);

        final Button buttonSaveParameters = findViewById(R.id.saveParameters);
        buttonSaveParameters.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(MarketParametersActivity.this, AnalysisActivity.class);

                String gapMin = ((EditText) findViewById(R.id.gapMin)).getText().toString();
                String gapMax = ((EditText) findViewById(R.id.gapMax)).getText().toString();
                String buyTarget = ((EditText) findViewById(R.id.buyTarget)).getText().toString();
                String sellTarget = ((EditText) findViewById(R.id.sellTarget)).getText().toString();
                String stopLoss = ((EditText) findViewById(R.id.stopLoss)).getText().toString();
                String timeWindow = ((EditText) findViewById(R.id.timeWindow)).getText().toString();

                if(gapMin.isEmpty() || gapMax.isEmpty() || buyTarget.isEmpty() || sellTarget.isEmpty() || stopLoss.isEmpty() || timeWindow.isEmpty()){
                    //Show Toast
                    showToast("Fill all fields to save!");
                }else {

                    SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("gapMin", gapMin);
                    editor.putString("gapMax", gapMax);
                    editor.putString("buyTarget", buyTarget);
                    editor.putString("sellTarget", sellTarget);
                    editor.putString("stopLoss", stopLoss);
                    editor.putString("timeWindow", timeWindow);
                    editor.apply();

                    //Show Toast
                    showToast("Saving Market Params...");

                    startActivity(intent);
                }

            }
        });
    }

    /**
     * Show toast messages
     * Auxiliar method to show any toast messages
     * with fixed duration of "LENGTH_LONG"
     */
    private void showToast(String message){
        //Show Toast
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }
}
