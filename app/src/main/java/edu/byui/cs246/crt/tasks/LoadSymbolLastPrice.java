package edu.byui.cs246.crt.tasks;

import android.os.AsyncTask;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.TickerStatistics;
import edu.byui.cs246.crt.AnalysisActivity;


/**
 * This class retrieves the last price of currency from Binance API
 */

public class LoadSymbolLastPrice extends AsyncTask<Void, TickerStatistics, Double> {

    AnalysisActivity parent;
    String symbol;
    TickerStatistics stats;
    TextView text;

    private static final Double MILLION = 1000000.0;


    public LoadSymbolLastPrice(AnalysisActivity parent, String symbol, TextView text)
    {
        this.parent = parent;
        this.symbol = symbol;
        this.text = text;
    }

    @Override
    protected Double doInBackground(Void... params) {

        try {

            BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance();
            BinanceApiRestClient client = factory.newRestClient();
            TickerStatistics priceStats = client.get24HrPriceStatistics(symbol);

            if (priceStats != null){
                Log.i("PRICE",priceStats.getLastPrice());
                publishProgress(priceStats);
                return Double.parseDouble(priceStats.getLastPrice());
            }

            //in case of error I return a MILLION to be studid high
            return MILLION;

        }catch (Exception e){
            Log.e("PRICE","[ERROR] " + e.getMessage());
            return MILLION;
        }

    }

    @Override
    protected void onPostExecute(Double result)
    {
        // call an external function as a result
        parent.ReturnThreadResult(result);
    }

    protected void onProgressUpdate(TickerStatistics... data) {
        stats = data[0];

        if (stats != null) {
            Double lastPrice = Double.parseDouble(stats.getLastPrice());
            text.setMovementMethod(new ScrollingMovementMethod());

            text.append(symbol + "  current price $" + lastPrice.toString() + "\n");
            final int scrollAmount = text.getLayout().getLineTop(text.getLineCount()) - text.getHeight();

            // if there is no need to scroll, scrollAmount will be <=0
            if (scrollAmount > 0) {
                text.scrollTo(0, scrollAmount);
            } else {
                text.scrollTo(0, 0);
            }
        }
    }


}