package edu.byui.cs246.crt.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.TickerStatistics;


/**
 * This class retrieves the currency last 24hrs information from Binance API
 */

public class LoadSymbol24HrPrice extends AsyncTask<Void, TickerStatistics, Void> {

    Context context;
    String symbol;
    TickerStatistics stats;
    ArrayAdapter<String> adapter;
    TextView current;

    /**
     * Retrieves the currency last 24hrs information from Binance API. Constructor Class
     * @param cont Context
     * @param eSymbol Currency pair
     * @param eAdapter Adapter
     * @param cp Text area
     */
    public LoadSymbol24HrPrice(Context cont, String eSymbol, ArrayAdapter<String> eAdapter, TextView cp)
    {
        context = cont;
        symbol = eSymbol;
        adapter = eAdapter;
        current = cp;
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {

            BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance();
            BinanceApiRestClient client = factory.newRestClient();
            TickerStatistics priceStats = client.get24HrPriceStatistics(symbol);

            if (priceStats != null){
                Log.i("PRICE",priceStats.getOpenPrice());
                publishProgress(priceStats);
            }

        }catch (Exception e){
            Toast toast = Toast.makeText(context, "Oops! something went wrong", Toast.LENGTH_SHORT);
            toast.show();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {

        Toast toast = Toast.makeText(context, "Loading values for " + symbol, Toast.LENGTH_SHORT);
        toast.show();
    }


    protected void onProgressUpdate(TickerStatistics... data) {
        stats = data[0];

        current.setText(stats.getLastPrice());
        adapter.add("Ask Price " + stats.getAskPrice());
        adapter.add("Bid Price " + stats.getBidPrice());
        adapter.add("High Price " + stats.getHighPrice());
        adapter.add("Low Price " + stats.getLowPrice());
        adapter.add("Previous Close Price " + stats.getPrevClosePrice());
        adapter.add("Open Price " + stats.getOpenPrice());
        adapter.add("Close Price " + stats.getCloseTime());
        adapter.add("Change " + stats.getPriceChangePercent() + "%");
        adapter.add("Volume Traded " + stats.getVolume());
        adapter.add("Average Price " + stats.getWeightedAvgPrice());
    }

    @Override
    protected void onPostExecute(Void result) {
        Toast toast = Toast.makeText(context, symbol + " values loaded", Toast.LENGTH_SHORT);
        toast.show();
    }


}