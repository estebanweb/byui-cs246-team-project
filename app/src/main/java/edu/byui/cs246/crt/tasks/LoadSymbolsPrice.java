package edu.byui.cs246.crt.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.TickerPrice;

import java.util.List;

import edu.byui.cs246.crt.Config;

/**
 * This class loads all currencies prices
 */

public class LoadSymbolsPrice extends AsyncTask<Void, TickerPrice, Void> {

    Context context;
    ProgressBar progressBar;
    ArrayAdapter<TickerPrice> adapter;
    int totalSize;

    public LoadSymbolsPrice(Context cont, ProgressBar bar, ArrayAdapter<TickerPrice> eAdapter)
    {
        progressBar = bar;
        context = cont;
        adapter = eAdapter;
        totalSize = 100;

    }

    @Override
    protected Void doInBackground(Void... params) {

        try {

            BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance();
            BinanceApiRestClient client = factory.newRestClient();
            List<TickerPrice> allPrices = client.getAllPrices();

            totalSize = allPrices.size();

            for (TickerPrice price : allPrices) {
                if (Config.ENABLED_SYMBOLS.contains(price.getSymbol())) {
                    publishProgress(price);
                }
            }

        }catch (Exception e){
            Toast toast = Toast.makeText(context, "Oops something went wrong", Toast.LENGTH_SHORT);
            toast.show();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {

        progressBar.setProgress(0);

        Toast toast = Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT);
        toast.show();
    }


    protected void onProgressUpdate(TickerPrice... data) {
        adapter.add(data[0]);
        progressBar.setProgress((adapter.getCount()*100)/totalSize);
    }

    @Override
    protected void onPostExecute(Void result) {
        progressBar.setProgress(100);
        Toast toast = Toast.makeText(context, "Exchange rates successfully loaded.", Toast.LENGTH_SHORT);
        toast.show();
    }


}