package edu.byui.cs246.crt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.binance.api.client.domain.market.TickerStatistics;
import java.util.ArrayList;
import edu.byui.cs246.crt.tasks.LoadSymbol24HrPrice;

/**
 * This class initiates the currency Details feature screen.
 */

public class DetailActivity extends AppCompatActivity {

    /**
     * Creates the Details screen
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_s_sign);
        }

        Intent intent = getIntent();
        String symbol = intent.getStringExtra("symbol");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(symbol);

        TickerStatistics stats = new TickerStatistics();

        TextView currentPrice = findViewById(R.id.currentPriceText);
        currentPrice.setText(stats.getLastPrice());

        ArrayList<String> valuesArray = new ArrayList<>();


        ArrayAdapter<String> pvAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.simple_list, R.id.textview, valuesArray);

        ListView priceValues = findViewById(R.id.priceValuesListView);
        priceValues.setAdapter(pvAdapter);

        new LoadSymbol24HrPrice(getApplicationContext(),symbol,pvAdapter,currentPrice).execute();

    }

}
