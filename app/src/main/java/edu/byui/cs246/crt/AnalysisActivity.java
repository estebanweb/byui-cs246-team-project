package edu.byui.cs246.crt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.binance.api.client.domain.market.TickerPrice;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import edu.byui.cs246.crt.tasks.PerformMarketAnalysis;

/**
 * Creates the screen for the main function of the app, the Market Analysis
 */
public class AnalysisActivity extends AppCompatActivity {

    protected Spinner currencyFrom;
    protected Gson gson;
    protected ArrayList<TickerPrice> listPrices;
    protected TextView textLog;
    protected Double lastPrice = 0.0;
    protected ArrayList priceArray = new ArrayList();
    private PerformMarketAnalysis runningMarketAnalysis = null;
    String selectedCoinPair = "";

    //TODO: Parameters from screeen, to be defined by the user
    double gapMin; //Minimum Diference between lowest and highest Price represented in percentage
    double gapMax; //Maximum Diference between lowest and highest Price represented in percentage
    double stopLoss ; //Percentage of how much the price can go down from buying price. This is the protection, if the prices goes hits this limit, sell and assume the loss, to avoid bigger loss
    double buyTarget; //Percentage of how much the buying price can be higher than the lowestPrice
    double sellTarget; //Percentage of how much the price must go to perform a sell.
    int timeWindow; //Time windows for analysis, in minutes

    /**
     * Create the Analisys Activity Screen
     * Load default parameters for Market Analysis
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
        }
        actionbar.setHomeAsUpIndicator(R.drawable.ic_s_sign);

        SharedPreferences sharedpreferences = getSharedPreferences("SYMBOLVALUES", Context.MODE_PRIVATE);
        String values = sharedpreferences.getString("VALUES",null);

        //Define Default Parameters
        SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("gapMin", "0.3");
        editor.putString("gapMax", "1.5");
        editor.putString("buyTarget", "0");
        editor.putString("sellTarget", "0.7");
        editor.putString("stopLoss", "1");
        editor.putString("timeWindow", "1");
        editor.apply();

        Type type = new TypeToken<List<TickerPrice>>() {}.getType();
        gson = new Gson();

        listPrices =  gson.fromJson(values, type);

        List<String> list = new ArrayList<>();

        for (TickerPrice price: listPrices) {
            list.add(price.getSymbol());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        currencyFrom = findViewById(R.id.currencyFrom);
        currencyFrom.setAdapter(adapter);


        lastPrice = 0.0;

        //Load Buttons
        final Button buttonMarketParams = findViewById(R.id.marketParamsButton);
        final Button buttonStart = findViewById(R.id.startButton);
        final Button buttonStop = findViewById(R.id.stopButton);

        //PARAMS button click listener
        buttonMarketParams.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(AnalysisActivity.this, MarketParametersActivity.class);
                AnalysisActivity.this.startActivity(myIntent);

            }
        });

        //START button click listener
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                textLog = findViewById(R.id.analysisText);

                selectedCoinPair = listPrices.get(currencyFrom.getSelectedItemPosition()).getSymbol();
                textLog.setText("Starting analysis for " + selectedCoinPair + "\n");

                SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams",Context.MODE_PRIVATE);
                String setParams = sharedPref.getString("setParams","") + "";
                String gapMinDef = sharedPref.getString("gapMin","");
                String gapMaxDef = sharedPref.getString("gapMax","");
                String stopLossDef = sharedPref.getString("stopLoss","");
                String buyTargetDef = sharedPref.getString("buyTarget","");
                String sellTargetDef = sharedPref.getString("sellTarget","");
                String timeWindowDef = sharedPref.getString("timeWindow","");
                //Set Analysis FLAG running
                editor.putString("isAnalysisRunning", "TRUE");
                editor.commit();

                gapMin = Double.valueOf(gapMinDef);
                gapMax = Double.valueOf(gapMaxDef);
                stopLoss = Double.valueOf(stopLossDef);
                buyTarget = Double.valueOf(buyTargetDef);
                sellTarget = Double.valueOf(sellTargetDef);
                timeWindow = Integer.valueOf(timeWindowDef);

                buttonStart.setEnabled(false);
                buttonStop.setEnabled(true);
                buttonMarketParams.setEnabled(false);

                startAnalysis(selectedCoinPair,textLog);

            }
        });

        //STOP button click listener
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("isAnalysisRunning", "FALSE");
                editor.apply();

                textLog = findViewById(R.id.analysisText);

                selectedCoinPair = listPrices.get(currencyFrom.getSelectedItemPosition()).getSymbol();
                textLog.setText("Stop analysis for " + selectedCoinPair + "\n");
                stopAnalysis();

                buttonStart.setEnabled(true);
                buttonStop.setEnabled(false);
                buttonMarketParams.setEnabled(true);

            }
        });
    }

    public void ReturnThreadResult (Double value){
        priceArray.add(value);
        lastPrice = value;
    }


    /**
     * Start Market Analisys cycle, by calling the Analysis class PerformMarketAnalysis
     * @param symbol The name of the currency pair. Example: Bitcoin x Tether = BTCUSDT
     * @param textLog Text element in the screen, where the analysis information will shown
     */
    protected void startAnalysis(String symbol, TextView textLog){

        if(runningMarketAnalysis == null || runningMarketAnalysis.isCancelled()){
            runningMarketAnalysis = new PerformMarketAnalysis(AnalysisActivity.this, selectedCoinPair, timeWindow, sellTarget,
                    buyTarget, stopLoss, gapMax, gapMin, textLog);

            runningMarketAnalysis.execute();
        }


    }

    /**
     * Start Market Analisys cycle, by calling the Analysis class PerformMarketAnalysis
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

    }

    /**
     * Stop Market Analisys cycle
     */
    protected void stopAnalysis(){
        if(runningMarketAnalysis != null)
            runningMarketAnalysis.cancel(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Define navegability from the menu in upper right of the screen
     * @param item Menu object from the top right on the screen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_analysis) {
            return true;
        }

        if (id == R.id.action_convert) {
            Intent intent = new Intent(this, ConverterActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_historic) {
            Intent intent = new Intent(this, HistoricActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_refresh) {
            SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams",Context.MODE_PRIVATE);
            String isRunning = sharedPref.getString("isAnalysisRunning","") + "";

            if(isRunning.equals("TRUE")){
                //Show Toast
                Toast toast = Toast.makeText(getApplicationContext(), "Monitor Price is blocked when Market Analysis is running!", Toast.LENGTH_LONG);
                toast.show();
            }else {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
        }

        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
