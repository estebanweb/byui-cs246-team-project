package edu.byui.cs246.crt.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import edu.byui.cs246.crt.R;
import static edu.byui.cs246.crt.PurchaseHistoryContract.HistoryEntry;

/**
 * This class deals with the Operation Log cursor adapter
 */
public class HistoricCursorAdapter extends CursorAdapter {

    public HistoricCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    /**
     * The newView method is used to inflate a new view and return it,
     * you don't bind any data to the view at this point.
     * @param context Context
     * @param cursor Cursor
     * @param parent Parent ViewGroup
     * @return return the View
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.row_historic, parent, false);
    }

    /**
     * The bindView method is used to bind all data to a given view
     * such as setting the text on a TextView.
     *
     * @param view View
     * @param context Context
     * @param cursor Cursor
     */
    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView buyDate = view.findViewById(R.id.buyDate);
        TextView buyPrice = view.findViewById(R.id.buyPrice);
        TextView pair = view.findViewById(R.id.pair);
        TextView sellDate = view.findViewById(R.id.sellDate);
        TextView sellPrice = view.findViewById(R.id.sellPrice);
        TextView rate = view.findViewById(R.id.rate);

        // Extract properties from cursor and populate fields with extracted properties
        buyDate.setText(formatDateFromString("dd MMM HH:MM",cursor.getString(cursor.getColumnIndexOrThrow(HistoryEntry.COLUMN_NAME_BUYDATE))));
        buyPrice.setText("$" + cursor.getString(cursor.getColumnIndexOrThrow(HistoryEntry.COLUMN_NAME_BUYPRICE)));
        pair.setText(cursor.getString(cursor.getColumnIndexOrThrow(HistoryEntry.COLUMN_NAME_PAIR)));
        sellDate.setText(formatDateFromString("dd MMM HH:MM", cursor.getString(cursor.getColumnIndexOrThrow(HistoryEntry.COLUMN_NAME_SELLDATE))));
        sellPrice.setText("$" + cursor.getString(cursor.getColumnIndexOrThrow(HistoryEntry.COLUMN_NAME_SELLPRICE)));
        rate.setText(String.format("%.4f",1.00 - cursor.getDouble(cursor.getColumnIndexOrThrow(HistoryEntry.COLUMN_NAME_RATE)))+"%");
    }

    private static String formatDateFromString(String outputFormat, String inputDate){

        Date parsed;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", java.util.Locale.ENGLISH);
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.e("HISTORIC_ROW", "ParseException - dateFormat");
        }

        return outputDate;

    }
}