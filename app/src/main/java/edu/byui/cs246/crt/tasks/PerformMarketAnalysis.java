package edu.byui.cs246.crt.tasks;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.TickerStatistics;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import edu.byui.cs246.crt.PurchaseHistoryDbHelper;


/**
 * This class represents the main function of the app, the Market Analysis
 * The market analysis consists on continuous a cycle of verifying market
 * prices and parameters defined by the user (or accepting the default values).
 * In this cycle, the App will get information of the price and build an history
 * array of prices, saving the price each 5 seconds. The total array size is defined
 * by the "Time Window". Exemplo: if the Time Window is define of 1 minute, it will have
 * 12 values, 60seconds (1 minute) / 5seconds(price update default interval) = 12.
 * Once the array is completed, each 5 seconds the first price will be removed a the new
 * price will be added.
 * From this array, the lower and higher price will be separated. Based on parameters,
 * the app will warn the user the time to buy, when the price reaches a certain percentage
 * of the lower price.
 * Once the Buy order was issued, the app will warn the user when it is time to sell, when
 * the price reaches certain percentage over the buy price, in order to achieve profit.
 * However, the price can go down, in order to avoid great losses, a "stop loss" percentage
 * is also defined. If the price reaches this value, the app will warn the user to sell it.
 * Once the cycle is concluded with a sell, the operation is registered in the history database,
 * in which we can verify the history of operation and if it was profitable or not.
 * The user can stop the Analysis and change parameters at any time.
 * The user can also choose with coin currence pair desires to use.
 */

public class PerformMarketAnalysis extends AsyncTask<Void, Void, Void> {

    private static DecimalFormat df2 = new DecimalFormat("##.##");

    private BinanceApiClientFactory factory;
    private BinanceApiRestClient client;
    @SuppressLint("StaticFieldLeak")
    private TextView textLog;
    @SuppressLint("StaticFieldLeak")
    private Context context;

    private double highestPrice = 0;
    private double lowestPrice = 0;
    private boolean buyOrderIssued = false;
    private boolean buyOrderIssuedUserConfirm = false;
    private boolean sellOrderIssued = false;
    private boolean sellStopLossIssued = false;
    private String buyDate = null;
    private ArrayList<Double> priceArray;
    private double gapPercentage = 0;
    private String insideGapPercentage = "FALSE";
    private double lastPrice = 0;
    private double buyPriceDefined = 0;
    private double buyPriceReal = 0;
    private double sellPriceDefined = 0;
    private double sellPriceReal = 0;
    private double sellPriceStopLossDefined = 0;
    private double sellPriceStopLossReal = 0;
    private boolean priceArrayCompleted = false;


    private int threadSleep = 5000;
    private int threadSleepAfterStopLoss = 5000;

    private int timeWindowArraySize = 0;
    private int timeWindowFilled = 0;


    ///////////////////Parameters from screen, define by the user
    private double gapLowHighMin = 0; //Min difference between lowest and highest Price represented in percentage
    private double gapLowHighMax = 0; // Max Difference between lowest and highest Price represented in percentage
    private double stopLoss = 0; //Percentage of how much the price can go down from buying price. This is the protection, if the prices goes hits this limit, sell and assume the loss, to avoid bigger loss
    private double buyPriceTolerance = 0; //Percentage of how much the buying price will be higher than the lowestPrice
    private double sellPriceTolerance = 0; //Percentage of how much the price must go to perform a sell.
    private int timeWindow = 0; //Time windows for analysis, in minutes
    private String coinPair = ""; //Coin Pair that will be evaluated
    /////////////////////////////////////////////////////////////////////////

    /**
     * Constructor method for PerformMarketAnalysis.
     * Receives the parameters and start Market Analysis cycle
     * @param cont Context
     * @param coinPairUser Currency pair definition. Example - Bitcoin x Tether: BTCUSDT
     * @param timeWindowUser Time window within the analysis will be executed. In minutes.
     * @param sellPriceToleranceUser Percentage of how much the price must go to perform a sell.
     * @param buyPriceToleranceUser Percentage of how much the buying price will be higher than the lowestPrice
     * @param stopLossUser  Percentage of how much the price can go down from buying price. This is the protection, if the prices goes hits this limit, sell and assume the loss, to avoid bigger loss
     * @param gapLowHighMaxUser Max Difference between lowest and highest Price represented in percentage
     * @param gapLowHighMinUser Min difference between lowest and highest Price represented in percentage
     * @param textLog Text element in the screen, where the analysis information will shown
     */
    public PerformMarketAnalysis(Context cont, String coinPairUser, int timeWindowUser, double sellPriceToleranceUser,
                                 double buyPriceToleranceUser, double stopLossUser, double gapLowHighMaxUser, double gapLowHighMinUser, TextView textLog) {
        super();

        ///////////////////Parameters from screen, define by the user
        gapLowHighMin = gapLowHighMinUser;
        gapLowHighMax = gapLowHighMaxUser;
        stopLoss = stopLossUser;
        buyPriceTolerance = buyPriceToleranceUser;
        sellPriceTolerance = sellPriceToleranceUser;
        timeWindow = timeWindowUser;
        coinPair = coinPairUser;
        /////////////////////////////////////////////////////////////////////////

        //Define timeWindow array size
        timeWindowArraySize = (timeWindow * 60000)/threadSleep;
        context = cont;
        this.textLog = textLog;
        this.initializeAPI();
        priceArray = new ArrayList<>();
    }

    /**
     * Execute the Market Analysis in Background
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {


            while (true) {

                if(isCancelled()){
                    break;
                }

                //Analysis cycle:
                //1 - Get Highest and lowest price between the time interval. For now, we have only 24hs available
                //2 - Calculate the % gap between highest and lowest price.
                //3 - Compare calculate gap with parameter gapLowHigh. If calculated gap is less or equal gapLowHigh, meet conditions to continue. Otherwise, wait the interval (5s) a start from step 1.
                //4 - Check last price. If equal or less than lowestPrice acquired, alert BUY ORDER.
                //5 - Once a BUY ORDER is active, keep verifying the price each 5 second, until it reaches buying price plus sell price.
                //6 - If the price reaches buying price, alert SELL ORDER and register the operation in history data base. Start from step 1
                //7 - If the price reaches buying less stop loss, alert SELL ORDER STOP LOSS and register the operation in history data base. Start from step 1.


                //1 - Get highest and lowest price
                //2 - Calculate Gap
                updateTimeTablePriceArray();


                //3 - Verify GAP
                //System.out.println("############## 3 - Verify Market GAP");
                if(insideGapPercentage.equals("FALSE"))
                    continue;

                //4 - Check BUY ORDER
                if(lastPrice <= buyPriceDefined){
                    buyOrderIssued = true;
                    sellOrderIssued = false;
                    sellStopLossIssued = false;

                    buyPriceReal = lastPrice;
                    buyDate = new Date().toString();


                }else {
                    continue;
                }

                //5 - Verify SELLING conditions
                while (buyOrderIssued){

                    if(isCancelled())
                        break;

                    //Update time table Price array
                    updateTimeTablePriceArray();

                    //6 - Verify SELL ORDER
                    if(lastPrice >=  sellPriceDefined){
                        buyOrderIssued = false;
                        buyOrderIssuedUserConfirm = false;
                        sellOrderIssued = true;

                        sellPriceReal = lastPrice;

                        //Save operation in database
                        saveTradeLog();

                        break;


                        //7 - Verify SELL ORDER STOP LOSS
                    }else if(lastPrice <= sellPriceStopLossDefined){
                        buyOrderIssued = false;
                        buyOrderIssuedUserConfirm = false;
                        sellStopLossIssued = true;

                        sellPriceStopLossReal = lastPrice;

                        //Save operation in database
                        saveTradeLog();

                        Thread.sleep(threadSleepAfterStopLoss);
                        break;
                    }

                }


            }


        } catch (Exception e) {
            Log.i("",e.getMessage());
        }

        return null;
    }

    /**
     * Auxiliar class to make notification, vibrate and sound alert
     */
    private static void buildNotificationCommon(Context _context, String title) {

        //### Vibration Alert
        Vibrator v = (Vibrator) _context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(500);
        }

        //### Sound Alert
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(_context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Auxiliar class to configure the correct alert when the an operation is done.
     */
    private void operationAlert(String type){

        String title = "";
        String message = "";

        if(type.equals("buy")){
            buyOrderIssuedUserConfirm = true;
            title = "BUY ORDER";
            message = "Buy Now!!!\nPrice: " + getStringBuyPriceReal();
        }

        if(type.equals("sell")){
            sellOrderIssued = false;
            title = "SELL ORDER";
            message = "Sell Now!!!\nPrice: " + getStringSellPriceReal();
        }

        if(type.equals("stopLoss")){
            sellStopLossIssued = false;
            title = "SELL ORDER";
            message = "Stop Loss Limit Reached! Sell Now!!!\nPrice: " + getStringBuyPriceReal();
        }

        buildNotificationCommon(context,title);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * Initialize Binance API to make it ready to obtain currency prices
     */
    private void initializeAPI(){
        factory = BinanceApiClientFactory.newInstance();
        client = factory.newRestClient();
    }

    /**
     * Retrieve the last price. The currency is the one in which the class was initialized
     */
    private double getLastPrice(){

        while(true){
            try{
                TickerStatistics priceStats = client.get24HrPriceStatistics(coinPair);
                lastPrice = Double.parseDouble(priceStats.getLastPrice());
                return lastPrice;
            }catch(Exception e){
                //Show Toast
                Toast toast = Toast.makeText(context, "Failed to communicate with Binance!", Toast.LENGTH_SHORT);
                toast.show();

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

            }
        }


    }

    /**
     * Update time table array. Fill the array until it reach its maximum size
     * or remove the oldest value and adds the new value.
     */
    private void updateTimeTablePriceArray(){
        try {

            if(priceArray.size() < timeWindowArraySize){
                priceArrayCompleted = false;

                for(int i = 1; i <= timeWindowArraySize; i++){
                    timeWindowFilled = i;
                    double currentLastPrice = getLastPrice();
                    priceArray.add(currentLastPrice);
                    if(isCancelled())
                        break;

                    Thread.sleep(threadSleep);
                    publishProgress();
                }

            }else{
                priceArrayCompleted = true;
                Thread.sleep(threadSleep);
                priceArray.remove(0);
                priceArray.add(getLastPrice());
            }

            updatePriceArrayStats();
            publishProgress();


        } catch (InterruptedException e) {
            Log.i("Info","updateTimeTablePriceArray Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Updates the market information in screen each 5 seconds.
     */
    protected void onProgressUpdate(Void... values) {
        String message = "Analysing Market " + coinPair + "...\n" +
                "GAP Min: " + this.gapLowHighMin + "%\n" +
                "GAP Max: " + this.gapLowHighMax + "%\n" +
                "Buy Target: " + this.buyPriceTolerance + "% \n" +
                "Sell Target: " + this.sellPriceTolerance + "% \n" +
                "Stop Loss: " + this.stopLoss + "% \n" +
                "Time Window: " + this.timeWindow + " minute(s) \n\n";


        if(priceArrayCompleted){
            message += "Current Price: " + getStringLastPrice() + "\n\n" +
                    "Min Price: " + getStringLowestPrice() + "\n" +
                    "Max Price: " + getStringHighestPrice() + "\n\n" +
                    "Current GAP: " + getStringGapPercentage() + "% >>> Good GAP: " + insideGapPercentage + "\n" +
                    "Target MAX Buy Price: " + getStringBuyPriceDefined() + "\n" +
                    "Buy Order Issued: " + String.valueOf(buyOrderIssued).toUpperCase() + "\n\n";

            if(buyOrderIssued) {
                message += "BUY ORDERED!!! Buy Price: " + getStringBuyPriceReal() + "\n" +
                        "Target Sell Price: " + getStringSellPriceDefined() + "\n" +
                        "Stop Loss Price: " + getStringSellPriceStopLossDefined();
            }

        }else{

            message += "Current Price: " + getStringLastPrice() + "\nBuilding Time Chart: " + df2.format(((double) timeWindowFilled / (double) timeWindowArraySize)*100) + "%";
        }
        this.textLog.setText(message);

        //### Operation Alert
        if(buyOrderIssued && !buyOrderIssuedUserConfirm)
            operationAlert("buy");

        if(!buyOrderIssued && sellOrderIssued)
            operationAlert("sell");

        if(!buyOrderIssued && sellStopLossIssued)
            operationAlert("stopLoss");
    }

    /**
     * Save the operation log in database
     */
    private void saveTradeLog(){
        //insert into the log
        PurchaseHistoryDbHelper.insertNewRow(context,
                buyDate,
                new Date().toString(),
                coinPair,
                Double.toString(buyPriceReal),
                Double.toString(sellPriceReal),
                "OK",
                Double.toString(buyPriceReal/sellPriceReal)
        );
    }


    /**
     * Update the stats based on the information in array
     * The states updated are:
     * Highest price
     * Lowest price
     * GAP Percentage
     * Flag - inside GAP interval
     * Last Price
     * Buy price calculated
     * Sell price calculated
     * Sell price calculated Stop loss
     */
    private void updatePriceArrayStats(){

        updateHighestPrice();
        updateLowestPrice();
        updateGapPercentage();
        updateInsideGapPercentage();
        updateLastPrice();
        updateBuyPriceDefined();
        updateSellPriceDefined();
        updateSellPriceStopLossDefined();
    }

    /**
     * Update Lowest Price
     */
    private void updateLowestPrice(){
        lowestPrice = Collections.min(priceArray);
    }

    /**
     * Update Last Price
     */
    private void updateLastPrice(){
        lastPrice = getLastPrice();
    }

    /**
     * Update Highest Price
     */
    private void updateHighestPrice(){
        highestPrice = Collections.max(priceArray);
    }

    /**
     * Update GAP percentage
     */
    private void updateGapPercentage(){
        gapPercentage = (1 - (lowestPrice / highestPrice))*100;
    }

    /**
     * Update Flag inside  GAP interval
     */
    private void updateInsideGapPercentage(){

        if(gapPercentage < gapLowHighMin || gapPercentage > gapLowHighMax) {
            insideGapPercentage = "FALSE";
        }else{
            insideGapPercentage = "TRUE";
        }
    }

    /**
     * Update calculated buy price
     */
    private void updateBuyPriceDefined(){
        if(!buyOrderIssued){
            buyPriceDefined = lowestPrice * ((buyPriceTolerance/100)+1);
        }
    }

    /**
     * Update calculated sell price
     */
    private void updateSellPriceDefined(){
        if(buyOrderIssued){
            sellPriceDefined = buyPriceReal*((sellPriceTolerance/100) + 1);
        }else{
            sellPriceDefined = 0;
        }
    }

    /**
     * Update calculated sell price stop loss
     */
    private void updateSellPriceStopLossDefined(){
        if(buyOrderIssued){
            sellPriceStopLossDefined = buyPriceReal - (buyPriceReal*(stopLoss/100));
        }else{
            sellPriceStopLossDefined = 0;
        }
    }

    /**
     * Return lowest price in format with format #.##
     */
    private String getStringLowestPrice(){

        return df2.format(Collections.min(priceArray));
    }

    /**
     * Return highest price in format with format #.##
     */
    private String getStringHighestPrice(){
        return df2.format(Collections.max(priceArray));
    }

    /**
     * Return GAP percentage in string with format #.##
     */
    private String getStringGapPercentage(){
        return df2.format(gapPercentage);
    }

    /**
     * Return last price in string  with format #.##
     */
    private String getStringLastPrice(){
        return df2.format(lastPrice);
    }

    /**
     * Return calculated buy price in string  with format #.##
     */
    private String getStringBuyPriceDefined(){
        return df2.format(buyPriceDefined);
    }

    /**
     * Return calculated sell price in string with format #.##
     */
    private String getStringSellPriceDefined(){
        return df2.format(sellPriceDefined);
    }

    /**
     * Return calculated sell price stop loss in string with format #.##
     */
    private String getStringSellPriceStopLossDefined(){
        return df2.format(sellPriceStopLossDefined);
    }

    /**
     * Return real buy price in string with format #.##
     */
    private String getStringBuyPriceReal(){
        return df2.format(buyPriceReal);
    }

    /**
     * Return real sell price in string with format #.##
     */
    private String getStringSellPriceReal(){
        return df2.format(sellPriceReal);
    }

    /**
     * Return real sell price stop loss in string with format #.##
     */
    private String getStringSellPriceStopLossReal(){
        return df2.format(sellPriceStopLossReal);
    }


}
