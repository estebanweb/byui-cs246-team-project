package edu.byui.cs246.crt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import static edu.byui.cs246.crt.PurchaseHistoryContract.*;

/**
 * Helper to handle the database.
 * Created by esteban on 4/2/18.
 */
public class PurchaseHistoryDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "PurchaseHistory.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + HistoryEntry.TABLE_NAME + " (" +
                    HistoryEntry._ID + " INTEGER PRIMARY KEY," +
                    HistoryEntry.COLUMN_NAME_BUYDATE + " TEXT," +
                    HistoryEntry.COLUMN_NAME_SELLDATE + " TEXT," +
                    HistoryEntry.COLUMN_NAME_PAIR + " TEXT," +
                    HistoryEntry.COLUMN_NAME_BUYPRICE + " DECIMAL(10,2)," +
                    HistoryEntry.COLUMN_NAME_SELLPRICE + " DECIMAL(10,2)," +
                    HistoryEntry.COLUMN_NAME_RATE + " DECIMAL(10,2)," +
                    HistoryEntry.COLUMN_NAME_STATUS + " INT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + HistoryEntry.TABLE_NAME;

    public PurchaseHistoryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String[] projection = {
            BaseColumns._ID,
            HistoryEntry.COLUMN_NAME_BUYDATE,
            HistoryEntry.COLUMN_NAME_BUYPRICE,
            HistoryEntry.COLUMN_NAME_PAIR,
            HistoryEntry.COLUMN_NAME_SELLDATE,
            HistoryEntry.COLUMN_NAME_SELLPRICE,
            HistoryEntry.COLUMN_NAME_RATE
    };

    /**
     * The first time you need to create it
     * @param db DB name
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * When you upgrade the version, this ensure the consistency
     * @param db DB name
     * @param oldVersion Old version
     * @param newVersion new version
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * When you downgrade the version, this ensure the consistency
     * @param db DB name
     * @param oldVersion old version
     * @param newVersion new version
     */
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Insert a row in the database with the correct data
     *
     * @param context Context
     * @param buyDate Date when the buy order was given
     * @param sellDate Date when the sell order was given
     * @param coinPair Currency pair defined
     * @param buyPrice Price when the buy order was given
     * @param sellPrice Price when the sell order was given
     * @param status Status
     * @param rate Rate of the operation, in percentage. Show profit or loss
     */
    public static long insertNewRow(Context context, String buyDate, String sellDate, String coinPair,
                                   String buyPrice, String sellPrice, String status, String rate){

        PurchaseHistoryDbHelper mDbHelper = new PurchaseHistoryDbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(HistoryEntry.COLUMN_NAME_BUYDATE,buyDate);
        values.put(HistoryEntry.COLUMN_NAME_SELLDATE, sellDate);
        values.put(HistoryEntry.COLUMN_NAME_PAIR, coinPair);
        values.put(HistoryEntry.COLUMN_NAME_BUYPRICE,buyPrice);
        values.put(HistoryEntry.COLUMN_NAME_SELLPRICE,sellPrice);
        values.put(HistoryEntry.COLUMN_NAME_STATUS,status);
        values.put(HistoryEntry.COLUMN_NAME_RATE,rate);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(HistoryEntry.TABLE_NAME, null, values);
    }

    /**
     *
     * Return the cursor with the historic log
     *
     * @param context Context
     */
    public static Cursor getHistoricLog(Context context) {
        PurchaseHistoryDbHelper mDbHelper = new PurchaseHistoryDbHelper(context);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Filter results WHERE "Status" = 'OK'
        String selection = HistoryEntry.COLUMN_NAME_STATUS + " = ?";
        String[] selectionArgs = { "OK" };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = " cast(" + HistoryEntry.COLUMN_NAME_BUYDATE + " as REAL) DESC;";

        Cursor cursor = db.query(
                HistoryEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        return cursor;

    }
}
