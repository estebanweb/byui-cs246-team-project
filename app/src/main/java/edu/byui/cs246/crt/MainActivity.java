package edu.byui.cs246.crt;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.binance.api.client.domain.market.TickerPrice;
import com.google.gson.Gson;
import java.util.ArrayList;
import edu.byui.cs246.crt.adapters.SymbolArrayAdapter;
import edu.byui.cs246.crt.tasks.LoadSymbolsPrice;

/**
 * Main class, start with currency price monitoring
 */
public class MainActivity extends AppCompatActivity {

    protected TextView mTextView;
    protected ProgressBar progressBar;
    protected ListView lv;
    protected SymbolArrayAdapter symbolAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_s_sign);
        }


        //Set AnalysisActivity FLAG
        SharedPreferences sharedPref = getSharedPreferences("edu.byui.cs246.MarketParams", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("isAnalysisRunning", "FALSE");
        editor.apply();

        symbolAdapter = new SymbolArrayAdapter(getApplicationContext(), R.layout.rowlayout, new ArrayList<>());

        symbolAdapter.registerDataSetObserver(new DataSetObserver()
        {
            @Override
            public void onChanged()
            {

                SharedPreferences sharedpreferences = getSharedPreferences("SYMBOLVALUES", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();

                Gson gson = new Gson();

                String values =  gson.toJson(symbolAdapter.getValues());
                editor.putString("VALUES",values);
                editor.apply();
            }
        });

        lv = findViewById(R.id.rates);
        lv.setAdapter(symbolAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                TickerPrice row =  (TickerPrice) parent.getItemAtPosition(position);

                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("symbol",row.getSymbol());
                startActivity(intent);
            }
        });


        mTextView = findViewById(R.id.mainText);
        progressBar = findViewById(R.id.progressBar);

        new LoadSymbolsPrice(getApplicationContext(),progressBar,symbolAdapter).execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_analysis) {
            Intent intent = new Intent(this, AnalysisActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_convert) {
            Intent intent = new Intent(this, ConverterActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_historic) {
            Intent intent = new Intent(this, HistoricActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_refresh) {
            symbolAdapter.clear();
            new LoadSymbolsPrice(getApplicationContext(),progressBar,symbolAdapter).execute();
        }

        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

}
